<?
include_once('shdp/simple_html_dom.php');


main( );

function main( $argv = array(), $argc = 0 ){

	css();
	
	$jornais["folha_sp"]["url"] = "https://www.vercapas.com.br/capa/folha-de-s-paulo.html";
	$jornais["estadosp_sp"]["url"] = "https://www.vercapas.com.br/capa/o-estado-de-sao-paulo.html";
	$jornais["globo_rj"]["url"] = "https://www.vercapas.com.br/capa/o-globo.html";
	$jornais["supern_mg"]["url"] = "https://www.vercapas.com.br/capa/super-noticia.html";
	$jornais["estadom_mg"]["url"] = "https://www.vercapas.com.br/capa/estado-de-minas.html";
	$jornais["otempo_mg"]["url"] = "https://www.vercapas.com.br/capa/o-tempo.html";
	$jornais["agorasp_sp"]["url"] = "https://www.vercapas.com.br/capa/agora.html";
	$jornais["infoextra_rj"]["url"] = "https://www.vercapas.com.br/capa/extra.html";
	$jornais["diariogau_rs"]["url"] = "https://www.vercapas.com.br/capa/diario-gaucho.html";
	$jornais["correiodp_rs"]["url"] = "https://www.vercapas.com.br/capa/correio-do-povo.html";
	$jornais["meiahora_rj"]["url"] = "https://www.vercapas.com.br/capa/meia-hora.html";
	$jornais["valore_sp"]["url"] = "https://www.vercapas.com.br/capa/valor-economico.html";
	$jornais["odia_rj"]["url"] = "https://www.vercapas.com.br/capa/o-dia.html";
	$jornais["atribuna_es"]["url"] = "https://www.vercapas.com.br/capa/a-tribuna.html";
	$jornais["correobraz_df"]["url"] = "https://www.vercapas.com.br/capa/correio-braziliense.html";
	$jornais["jornalnh_rs"]["url"] = "https://www.vercapas.com.br/capa/jornal-nh.html";
	$jornais["diariodsp_sp"]["url"] = "https://www.vercapas.com.br/capa/diario-de-sao-paulo.html";
	$jornais["dezmin_am"]["url"] = "https://www.vercapas.com.br/capa/dez-minutos.html";
	$jornais["diariodper_pe"]["url"] = "https://www.vercapas.com.br/capa/diario-de-pernambuco.html";
	$jornais["folhamdper_pe"]["url"] = "https://www.vercapas.com.br/capa/folha-de-pernambuco.html";
	$jornais["jorndocom_pe"]["url"] = "https://www.vercapas.com.br/capa/jornal-do-commercio.html";
	$jornais["atarde_ba"]["url"] = "https://www.vercapas.com.br/capa/a-tarde.html";

	foreach( $jornais as $key => $value ){
		$url = $value[ "url" ];
		loguear( "get $key:$url" );

		$html = file_get_html( $url, false, null, 0 );
		//echo $html;

		$es = $html->find( 'div.scover', 0 )->find( 'figure', 0)->find('img', 0);
		echo "<img src=" . $es->src . "><hr>\n";

	}

}

//------------------------------------------------
// loguear
//------------------------------------------------
function loguear( $que, $tipo = "inform", $display = 1, $rendlog = 0, $nomail = 0 ){

	$filename = "capas.log";
	if( !$fp = fopen($filename, 'a+' ) ){
		echo "no se puede abrir w+ el archivo $fileName";
	}
	else {
		$fecha = date( "Ymd H:i:s" );

		fwrite( $fp, "$fecha $que\n" );
		if( $display ){
			$cls = "log $tipo";
			echo "<div class=' " . $cls . "'>$que</div>";
		}
		fclose( $fp );
	}

	if( $tipo == "error" )
		$error = 1;
	else
		$error = 0;

	if( $rendlog ){
		log_insertar( "$fecha $que", $error );
	}

	if( $tipo == "error" && ! $nomail ){
		$ret = reportar( "dms.gti@ecogas.com.ar", "obiazzi@ecogas.com.ar", "$fecha $que", "rend.php " . substr( $que, 0, 50 ) );
		if( $ret != "ok" ){
			//recursivo pero sin envio de mail ya que esta fallanado
			//por seguridad recursividad hacemos s-leep
			$sleepsec = 60;
			sleep($sleepsec);
			//        c-encuyx, q-ue,                                                  t-ipo  $d-isp, l-og  n-omail
			loguear( "rend.loguear: envio de mails no volvio ok sino $ret", "error", 1,     1,    1 );
		}
	}

	return;
}

function reportar( $de, $a, $subj, $que ){
	return "ok";
}

function css(){
echo <<<FINN
<script src="jquery-3.3.1.min.js"></script>
<style>
.log {
	white-space: pre-wrap;
}
.error {
	color: red;
}
</style>
FINN;
}

?>