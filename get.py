#!/usr/bin/python

#ejemplo ./get.py cxo 2010-01-01 2020-02-29 > cxo.csv
#insert into asset( tck, den, sect, tipo, mkt ) values ( 'cxo', 'concho resources', 'petroleo', 'acc', 'nyse' );
#create temporary table tmp_yf( tck,fec,open,max,min,close,vol );

#.separator "#"
#.import cxo.csv tmp_yf

#insert into as_pr( tck, fec, open, max, min, close, vol )
#select y.tck, y.fec, y.open, y.max, y.min, y.close, y.vol from tmp_yf y;


import yfinance as yf
import sys

tn = sys.argv[1]
d = sys.argv[2]
h = sys.argv[3]

tck = yf.Ticker( tn )

h = tck.history( start=d, end=h )

#(Timestamp('2020-02-28 00:00:00'),
#Open                 61.43
#High                 68.14
#Low                  61.42
#Close                68.02
#Volume          3787200.00
#Dividends             0.00
#Stock Splits          0.00

#insert into as_pr( tck, fec, open, max, min, close, vol )

for index, row in h.iterrows():
     print tn +"#" + index.strftime("%Y-%m-%d" ) + "#" + str( row["Open"] ) + "#" + str( row["High"] ) + "#" + str( row["Low"] ) + "#" + str( row["Close"] ) + "#" + str( row["Volume"] )
 
     
#
#for key, value in h.items():
#    print( key )
#exit()
#
#for key, value in h["Dividends"].items():
#    if value != 0.0:
#        print(key, value)
